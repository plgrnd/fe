# FE
Express.js dummy frontend.

## Development
```shell
docker compose build
docker compose up
curl -X POST localhost:8000
curl localhost:8000
```

## Endpoints
- GET /
- POST /
- PUT /
- PATCH /

## OpenAPI
- [Spec](localhost:8000/v3/api-docs)
- [Swagger UI](localhost:8000/swagger-ui.html) 
