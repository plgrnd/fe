FROM node:18.16.1-slim
WORKDIR /workspace/app
COPY package.json .
COPY app.js .
ENV NPM_CONFIG_REGISTRY=https://registry.npmjs.org
RUN npm install --save @opentelemetry/api && \
    npm install --save @opentelemetry/auto-instrumentations-node && \
    npm install
EXPOSE 8000
USER node:node
HEALTHCHECK CMD curl --fail http://localhost:8000 || exit 1
ENV NODE_OPTIONS "--require @opentelemetry/auto-instrumentations-node/register"
CMD ["node", "app.js"]