const express = require("express"),
    http = require("http"),
    app = express();
const packageJson = require('./package.json'),
    swaggerJsdoc = require('swagger-jsdoc'), /**/
    swaggerUi = require('swagger-ui-express');

function proxy(req, res) {
    const options = {
        host: process.env.BACKEND_HOST || "backend",
        port: process.env.BACKEND_PORT || 9000,
        path: req.path,
        method: req.method,
    }
    http.request(options, resp => {
        resp.on("data", data => {
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.send(data);
        });
    }).on("error", err => {
        console.log("Error: " + err.message);
    }).end();
}

/**
 * @swagger
 * /:
 *   get:
 *     summary:
 *     responses:
 *       200:
 *         description: Success
 *   post:
 *     summary:
 *     responses:
 *       200:
 *         description: Success
 *   put:
 *     summary:
 *     responses:
 *       200:
 *         description: Success
 *   patch:
 *     summary:
 *     responses:
 *       200:
 *         description: Success
 */
app.get("/", function (req, res) {
    proxy(req, res);
});
app.post("/", function (req, res) {
    proxy(req, res);
});
app.put("/", function (req, res) {
    proxy(req, res);
});
app.patch("/", function (req, res) {
    proxy(req, res);
});

const openApiOptions = {
    swaggerDefinition: {
        info: {
            title: packageJson.name,
            version: packageJson.version,
        },
    },
    apis: ['./app.js'],
};
const openApiSpec = swaggerJsdoc(openApiOptions);
app.get('/v3/api-docs', (req, res) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.send(openApiSpec);
});
app.use('/swagger-ui.html', swaggerUi.serve, swaggerUi.setup(openApiSpec));

http.createServer(app).listen(8000, "0.0.0.0", function () {
    console.log(`Server listening at ${this.address().address}:${this.address().port}`);
});